const initialState = {
    production_companies: [],
    budget: 0,
    revenue: 0
}

const movieDetails = (state = initialState, action) => {
    switch (action.type) {
        case 'RECEIVE_MOVIE_DETAILS':
            return {
                ...state,
                ...action.payload
            }
        default:
            return state;
    }
}

export default movieDetails