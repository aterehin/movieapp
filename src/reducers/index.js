import {combineReducers} from 'redux'
import genres from './genres'
import movies from './movies'
import movieDetails from './movieDetails'
import movieSimilar from './movieSimilar'
import moviesSearch from './moviesSearch'

export default combineReducers({
    genres,
    movies,
    movieDetails,
    movieSimilar,
    moviesSearch
})