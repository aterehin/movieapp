const genres = (state = {}, action) => {
    switch (action.type) {
        case 'RECEIVE_GENRES':
            return action.payload.reduce((result, current) => {
                result[current.id] = current.name;
                return result
            }, {})
        default:
            return state;
    }
}

export default genres