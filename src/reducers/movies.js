const initialState = {
    results: [],
    page: 0,
    total_pages: 0
}

const movies = (state = initialState, action) => {
    switch (action.type) {
        case 'RECEIVE_MOVIES':
            return {
                ...state,
                ...action.payload
            }
        default:
            return state;
    }
}

export default movies;