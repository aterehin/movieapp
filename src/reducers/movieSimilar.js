const initialState = {
    results: []
}

const movieSimilar = (state = initialState, action) => {
    switch (action.type) {
        case 'RECEIVE_MOVIE_SIMILAR':
            return {
                ...state,
                ...action.payload
            }
        default:
            return state;
    }
}

export default movieSimilar