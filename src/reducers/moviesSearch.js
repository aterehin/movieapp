const initialState = {
    results: []
}

const moviesSearch = (state = initialState, action) => {
    switch (action.type) {
        case 'RECEIVE_MOVIES_SEARCH':
            return {
                ...state,
                ...action.payload
            }
        default:
            return state;
    }
}

export default moviesSearch