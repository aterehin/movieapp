const API_KEY = '266667c0b6ca45583303c67a7ca8469a';

export const receiveGenres = (genres) => ({
    type: 'RECEIVE_GENRES',
    payload: genres
})

export const receiveMovies = (result) => ({
    type: 'RECEIVE_MOVIES',
    payload: result
})

export const receiveMovieDetails = (movie) => ({
    type: 'RECEIVE_MOVIE_DETAILS',
    payload: movie
})

export const receiveMovieSimilar = (movie) => ({
    type: 'RECEIVE_MOVIE_SIMILAR',
    payload: movie
})

export const receiveMoviesSearch = (movies) => ({
    type: 'RECEIVE_MOVIES_SEARCH',
    payload: movies
})

export function fetchGenres() {
    return function (dispatch) {
        return fetch(`https://api.themoviedb.org/3/genre/movie/list?api_key=${API_KEY}&language=en-US`)
            .then(response => response.json())
            .then(json => dispatch(receiveGenres(json.genres)))
    }
}

export function fetchMovies(page = 1) {
    return function (dispatch) {
        return fetch(`https://api.themoviedb.org/3/movie/popular?api_key=${API_KEY}&page=${page}`)
            .then(response => response.json())
            .then(json => dispatch(receiveMovies(json)))
    }
}

export function fetchMovieDetails(id) {
    return function (dispatch) {
        return fetch(`https://api.themoviedb.org/3/movie/${id}?api_key=${API_KEY}`)
            .then(response => response.json())
            .then(json => dispatch(receiveMovieDetails(json)))
    }
}

export function fetchMovieSimilar(id) {
    return function (dispatch) {
        return fetch(`https://api.themoviedb.org/3/movie/${id}/similar?api_key=${API_KEY}`)
            .then(response => response.json())
            .then(json => dispatch(receiveMovieSimilar(json)))
    }
}

export function fetchMoviesSearch(query) {
    return function (dispatch) {
        return fetch(`https://api.themoviedb.org/3/search/movie?api_key=${API_KEY}&query=${query}`)
            .then(response => response.json())
            .then(json => dispatch(receiveMoviesSearch(json)))
    }
}
