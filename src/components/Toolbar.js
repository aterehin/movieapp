import React from 'react'
import { Link } from 'react-router-dom'
import MoviesSearch from '../containers/MoviesSearch'

const LOGO_URL = 'https://www.themoviedb.org/assets/static_cache/27b65cb40d26f78354a4ac5abf87b2be/images/v4/logos/powered-by-rectangle-green.svg';

const Toolbar = () => (
    <header className="header">
        <div className="container">
            <div className="row">
                <div className="col-md-6">
                    <Link to="/" className="logo">
                        <img src={LOGO_URL} alt="The Movie Database"/>
                        <p>Movies App</p>
                    </Link>
                </div>
                <div className="col-md-6">
                    <MoviesSearch/>
                </div>
            </div>
        </div>
    </header>
)

export default Toolbar