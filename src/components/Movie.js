import React, { Component } from 'react'
import { Link } from 'react-router-dom'

export default class Movie extends Component {
    render() {
        const { movie: {
            id,
            title,
            release_date,
            poster_path,
            genre_ids
        }, genres } = this.props

        return (
            <div className="col-lg-2">
                <div className="movies_item">
                    <Link to={`/movie/${id}`}>
                        <img src={`https://image.tmdb.org/t/p/w300${poster_path}`} alt={title}/>
                        <h5>{title}</h5>
                        <p>{new Date(release_date).getFullYear()}, {genre_ids.map(id => genres[id]).join(', ')}</p>
                    </Link>
                </div>
            </div>
        )
    }
}