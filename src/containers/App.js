import React, { Component } from 'react'
import { connect } from 'react-redux'
import { BrowserRouter as Router, Route } from 'react-router-dom'
import { fetchGenres } from '../actions'
import Movies from './Movies'
import MovieDetails from './MovieDetails'
import Toolbar from '../components/Toolbar'

class App extends Component {
    componentDidMount() {
        this.props.getGenres()
    }
    render() {
        return (
            <Router>
                <div>
                    <Toolbar />
                    <div className="container">
                        <Route exact path="/(page/[0-9]+)?" component={Movies} />
                        <Route path="/movie/:id" component={MovieDetails}/>
                    </div>
                </div>
            </Router>
        )
    }
}

const mapDispatchToProps = dispatch => ({
    getGenres: () => dispatch(fetchGenres())
});

export default connect(null, mapDispatchToProps)(App)