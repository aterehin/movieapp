import React, { Component } from 'react'
import { connect } from 'react-redux'
import { fetchMovies } from '../actions'
import Movie from '../components/Movie'
import Pagination from "react-js-pagination";

class Movies extends Component {
    constructor(props) {
        super(props)

        this.handlePageChange = this.handlePageChange.bind(this)
    }

    componentDidMount(){
        const { match: { params }, getMovies } = this.props

        getMovies(this.getPage(params[0]))
    }

    componentWillReceiveProps(props) {
        const { match: { params: oldParams }, getMovies } = this.props
        const { match: { params: newParams } } = props

        if(newParams[0] !== oldParams[0]) {
            getMovies(this.getPage(newParams[0]))
        }
    }

    handlePageChange(page) {
        this.props.history.push('/page/' + page)
    }

    getPage(params) {
        return params && params.replace('page/','')
    }

    render() {
        const { movies: {results, page, total_results}, genres } = this.props

        return (
            <div className="movies">
                <Pagination itemClass="page-item"
                            linkClass="page-link"
                            prevPageText="Previous"
                            nextPageText="Next"
                            firstPageText="First"
                            lastPageText="Last"
                            activePage={page}
                            itemsCountPerPage={20}
                            pageRangeDisplayed={5}
                            totalItemsCount={total_results}
                            onChange={this.handlePageChange} />
                <div className="row">
                    {results.map(movie =>
                        <Movie key={movie.id} movie={movie} genres={genres} />
                    )}
                </div>
                <Pagination itemClass="page-item"
                            linkClass="page-link"
                            prevPageText="Previous"
                            nextPageText="Next"
                            firstPageText="First"
                            lastPageText="Last"
                            activePage={page}
                            itemsCountPerPage={20}
                            pageRangeDisplayed={5}
                            totalItemsCount={total_results}
                            onChange={this.handlePageChange} />
            </div>
        )
    }
}

const mapStateToProps = state => ({
    movies: state.movies,
    genres: state.genres
});
const mapDispatchToProps = dispatch => ({
    getMovies: (page) => dispatch(fetchMovies(page))
});

export default connect(mapStateToProps, mapDispatchToProps)(Movies)