import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import { fetchMoviesSearch, receiveMoviesSearch } from '../actions'

class MoviesSearch extends Component {
    constructor(props){
        super(props)

        this.handleChange = this.handleChange.bind(this)
        this.handleClickOutside = this.handleClickOutside.bind(this)
    }

    componentDidMount() {
        document.addEventListener('mousedown', this.handleClickOutside);
    }

    componentWillUnmount() {
        document.removeEventListener('mousedown', this.handleClickOutside);
    }

    handleChange() {
        const value = this.inputNode.value

        if(this.timeout) {
            clearTimeout(this.timeout);
        }

        if(value !== '') {
            this.timeout = setTimeout(() => {
                this.props.getMoviesSearch(value)
            }, 300)
        } else {
            this.clearSearchResults()
        }
    }

    handleClickOutside(event) {
        if (this.wrapperNode && !this.wrapperNode.contains(event.target)) {
            this.clearSearchResults()
        }
    }

    clearSearchResults() {
        this.props.clearMoviesSearch({
            page: null,
            results: [],
            total_pages: null,
            total_results: null
        })
    }

    render() {
        const { movies: { results }, genres } = this.props
        return (
            <div className="header_autocomplete" ref={(node) => { this.wrapperNode = node }}>
                <input type="text" ref={(node) => { this.inputNode = node; }} onChange={this.handleChange} placeholder="Type here to search..."/>
                <ul className="header_autocomplete_results">
                    {results.slice(0, 10).map(movie =>
                        <li key={movie.id}>
                            <Link to={`/movie/${movie.id}`} onClick={() => this.clearSearchResults()}>
                                <dl>
                                    <dt>{movie.title}</dt>
                                    <dd>{new Date(movie.release_date).getFullYear()}, {movie.genre_ids.map(id => genres[id]).join(', ')}</dd>
                                </dl>
                            </Link>
                        </li>
                    )}
                </ul>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    movies: state.moviesSearch,
    genres: state.genres
});
const mapDispatchToProps = dispatch => ({
    getMoviesSearch: (query) => dispatch(fetchMoviesSearch(query)),
    clearMoviesSearch: (movies) => dispatch(receiveMoviesSearch(movies))
});

export default connect(mapStateToProps, mapDispatchToProps)(MoviesSearch)