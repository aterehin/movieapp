import React, { Component } from 'react'
import { connect } from 'react-redux'
import scrollToElement from 'scroll-to-element'
import { fetchMovieDetails, fetchMovieSimilar } from '../actions'
import Movie from '../components/Movie'

class MovieDetails extends Component {
    componentDidMount() {
        const { match: { params }, getMovieDetails, getMovieSimilar } = this.props

        getMovieDetails(params.id);
        getMovieSimilar(params.id);
    }

    componentDidUpdate(){
        const { movie: { backdrop_path: path } } = this.props

        if(path) {
            document.body.style.backgroundImage = `linear-gradient(45deg, rgba(9, 28, 37, 0.925) 0%, rgba(1, 172, 98, 0.925) 100%), 
                                            url('https://image.tmdb.org/t/p/original${path}')`
        }
    }

    componentWillUnmount(){
        document.body.style.backgroundImage = null
    }

    componentWillReceiveProps(props) {
        const { match: { params: oldParams }, getMovieDetails, getMovieSimilar } = this.props
        const { match: { params: newParams } } = props

        if(newParams.id !== oldParams.id) {
            scrollToElement(this.movieNode, {
                offset: -80,
                ease: 'out-back',
                duration: 900
            });
            getMovieDetails(newParams.id)
            getMovieSimilar(newParams.id)
        }
    }

    render() {
        const {movie: {
            poster_path,
            title,
            tagline,
            overview,
            release_date,
            status,
            runtime,
            production_companies,
            budget,
            revenue
        }, similar: {
            results
        }, genres} = this.props

        return (
            <div className="movie" ref={(node) => { this.movieNode = node }}>
                <div className="movie_content">
                    <div className="row">
                        <div className="col-md-4">
                            <div className="movie_poster">
                                {poster_path &&
                                <img src={`https://image.tmdb.org/t/p/w500${poster_path}`} alt={title}/>
                                }
                            </div>
                        </div>
                        <div className="col-md-8">
                            <div className="movie_entry">
                                <h2>{title}</h2>
                                <p className="movie_tagline">{tagline}</p>
                                <p className="movie_overview">{overview}</p>
                                <div className="movie_details">
                                    <div className="row">
                                        <div className="col-md-5">
                                            <dl>
                                                <dt>Release Date:</dt>
                                                <dd>{release_date}</dd>
                                            </dl>
                                            <dl>
                                                <dt>Status:</dt>
                                                <dd>{status}</dd>
                                            </dl>
                                            <dl>
                                                <dt>Runtime:</dt>
                                                <dd>{runtime} mins</dd>
                                            </dl>
                                        </div>
                                        <div className="col-md-7">
                                            <dl>
                                                <dt>Production:</dt>
                                                <dd>{production_companies.map(company => company.name).join(', ')}</dd>
                                            </dl>
                                            <dl>
                                                <dt>Budget:</dt>
                                                <dd>{budget > 0 ? budget.toLocaleString() : '-'}</dd>
                                            </dl>
                                            <dl>
                                                <dt>Revenue:</dt>
                                                <dd>{revenue > 0 ? revenue.toLocaleString() : '-'}</dd>
                                            </dl>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {results.length &&
                    <div className="movie_similar">
                        <h3>You may also like:</h3>
                        <div className="row">
                            {results.map(movie =>
                                <Movie key={movie.id} movie={movie} genres={genres} />
                            )}
                        </div>
                    </div>
                }
            </div>
        )
    }
}

const mapStateToProps = state => ({
    movie: state.movieDetails,
    similar: state.movieSimilar,
    genres: state.genres
});
const mapDispatchToProps = dispatch => ({
    getMovieDetails: (id) => dispatch(fetchMovieDetails(id)),
    getMovieSimilar: (id) => dispatch(fetchMovieSimilar(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(MovieDetails)