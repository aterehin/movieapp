# MovieApp
An application based on the React library. Lets you search for information about movie in the TMDb database.

## Tools
Key tools used in this React project are:

| Tool             | Description   |
| :-------------:|--------------|
| [React](http://facebook.github.io/react/index.html) | A JavaScript library for building user interfaces |
| [Redux](https://redux.js.org/) | Redux is a predictable state container for JavaScript apps. |
| [Bootstrap](http://getbootstrap.com/) | Build responsive, mobile-first projects on the web with the world's most popular front-end component library |
| [SASS](http://sass-lang.com/) | 	Sass is the most mature, stable, and powerful professional grade CSS extension language in the world |
| [Babel](https://babeljs.io/) | Use next generation JavaScript, today |
| [Webpack](https://webpack.js.org/) | Webpack is a module bundler. Webpack takes modules with dependencies and generates static assets representing those modules |

## Installation
[node.js](http://nodejs.org/download/) is required to get ``npm``.

If you would like to download the code and try it for yourself:

1. Clone the repo: `git clone git@bitbucket.org:aterehin/movieapp.git`
2. `cd movieapp`
2. Install packages: `npm install`
3. Build project and launch: `npm start`
4. Open your browser at: `http://localhost:8080`

## License
[MIT](https://bitbucket.org/aterehin/movieapp/src/master/LICENCE)